import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';

import 'package:http/http.dart' as http;

const kGoogleApiKey = 'AIzaSyCl7iLsGR0QdsV4vAeXlL7acDODuCDFHpc';
const sessionToken = 'testing1234';
final places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

Future<List<Autocomplete>> getAutoComplete(String predict, String type) async {
  PlacesAutocompleteResponse response = await places.autocomplete(
    predict,
    sessionToken: sessionToken,
  );

  List<Autocomplete> list = [];

  if (response.isOkay) {
    for (var predictions in response.predictions) {
      list.add(Autocomplete(
          description: predictions.description,
          id: predictions.id,
          placeId: predictions.placeId,
          reference: predictions.reference));
    }
  }
  return list;
}

Future<PlaceDetails> getPlaces(String placeId) async {
  PlacesDetailsResponse response = await places.getDetailsByPlaceId(placeId);
  PlaceDetails result;
  if (response.isOkay) {
    result = response.result;
  }
  return result;
}

Future<Weather> getWeatherWithPlace(String placeId) async {
  PlacesDetailsResponse response = await places.getDetailsByPlaceId(placeId);
  Weather result;
  if (response.isOkay) {
    PlaceDetails placeDetails = response.result;
    final loc = placeDetails.geometry.location;
    result = await getDailyWeather(loc.lat, loc.lng, DateTime.now());
  }
  return result;
}

Future<Weather> getDailyWeather(double lat, double lng, DateTime dat) async {
  dat ?? DateTime.now();
  Weather result;
  final response = await (http
      .get("https://api.sunrise-sunset.org/json?lat=$lat&lng=$lng&date=$dat"));

  if (response.statusCode == 200) {
    final wr = (json.decode(response.body))
        .map((data) => new WeatherResponse.fromJson(data));
    result = wr.results;
  }

  return result;
}

class Autocomplete {
  final String description;
  final String id;
  final String placeId;
  final String reference;

  Autocomplete({this.description, this.id, this.placeId, this.reference});

  factory Autocomplete.fromJson(Map<String, dynamic> json) {
    return Autocomplete(
        description: json['description'],
        id: json['id'],
        placeId: json['place_id'],
        reference: json['reference']);
  }
}

class WeatherResponse {
  final Weather results;
  final String status;

  WeatherResponse({this.results, this.status});

  factory WeatherResponse.fromJson(Map json) {
    return WeatherResponse(
      results: Weather.fromJson(json['results']),
      status: json['status'],
    );
  }
}

class Weather {
  final String sunrise;
  final String sunset;
  final String solarNoon;
  final String dayLength;
  final String civilTwilightBegin;
  final String civilTwilightEnd;
  final String nauticalTwilightBegin;
  final String nauticalTwilightEnd;
  final String astronomicalTwilightBegin;
  final String astronomicalTwilightEnd;

  Weather(
      {this.sunrise,
      this.sunset,
      this.solarNoon,
      this.dayLength,
      this.civilTwilightBegin,
      this.civilTwilightEnd,
      this.nauticalTwilightBegin,
      this.nauticalTwilightEnd,
      this.astronomicalTwilightBegin,
      this.astronomicalTwilightEnd});

  factory Weather.fromJson(Map json) {
    return Weather(
      sunrise: json['sunrise'],
      sunset: json['sunset'],
      solarNoon: json['solar_noon'],
      dayLength: json['day_length'],
      civilTwilightBegin: json['civil_twilight_begin'],
      civilTwilightEnd: json['civil_twilight_end'],
      nauticalTwilightBegin: json['nautical_twilight_begin'],
      nauticalTwilightEnd: json['nautical_twilight_end'],
      astronomicalTwilightBegin: json['astronomical_twilight_begin'],
      astronomicalTwilightEnd: json['astronomical_twilight_end'],
    );
  }
}
