import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'api.dart';

void main() => runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    ));

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Photo Time'),
        backgroundColor: Colors.amberAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(context: context, delegate: DataSearch());
            },
          )
        ],
      ),
      drawer: Drawer(),
    );
  }
}

class DataSearch extends SearchDelegate<String> {
  String suggestion;

  @override
  List<Widget> buildActions(BuildContext context) {
    // actions for appbar
    return [IconButton(icon: Icon(Icons.clear), onPressed: () {})];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // leading icon on the left of the appbar
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // show result based on the selection
    return Container(
      child: FutureBuilder(
      future: getWeatherWithPlace(suggestion),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) return new Container();
        PlaceDetails content = snapshot.data;
        snapshot = null;
        return Card(
          color: Colors.blueAccent,
          child: Center(
            child: Text(content.geometry.location.lat.toString() + content.geometry.location.lng.toString()),
          ),
        );
      },
    ),);

    // return Card(
    //   color: Colors.lightBlueAccent,
    //   child: Center(
    //     child: Text(query),
    //   ),
    // );
  }

  // Widget getPlaceDetails(String placeId) {

  //    return FutureBuilder(
  //     future: getPlaces(placeId),
  //     builder: (BuildContext context, AsyncSnapshot snapshot) {
  //       if (!snapshot.hasData) return new Container();
  //       List content = snapshot.data;
  //       snapshot = null;

  //       return buildResults(context);
  //       /* return Card(
  //         color: Colors.blueAccent,
  //         child: Center(
  //           child: Text(content.first),
  //         ),
  //       ); */
  //     },
  //   );
  // }

  @override
  Widget buildSuggestions(BuildContext context) {
    // show when someone searches for something

    // final suggestionList = query.isEmpty ? recentSearch : cities;

    return FutureBuilder(
      future: query.isEmpty ? null : getAutoComplete(query, '0'),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        //if (snapshot.connectionState == ConnectionState.waiting) {
        if (!snapshot.hasData) return new Container();
        List content = snapshot.data;
        snapshot = null;
        return ListView.builder(
          itemBuilder: (context, index) => ListTile(
                onTap: () {
                  suggestion = content[index].placeId;
                  //getPlaceDetails(content[index].placeId);
                  showResults(context);
                },
                leading: Icon(Icons.location_city),
                title: RichText(
                  text: TextSpan(
                      text: content[index]
                          .description
                          .toString()
                          .substring(0, query.length),
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                      children: [
                        TextSpan(
                            text: content[index]
                                .description
                                .toString()
                                .substring(query.length),
                            style: TextStyle(color: Colors.blueAccent))
                      ]),
                ),
              ),
          itemCount: content.length,
        );
        //} else {
        //return CircularProgressIndicator();
        //}
      },
    );
  }
}
